#!/usr/bin/env python3
"""A script that montiors for powerloss and gracefully shutsdown the RPi.

This script monitors the power loss pin on the X708 v1.2 UPS for the
RaspberryPi. In the event a power loss event is detected, the script will
initiate an auto-shutdown routine.

Details on the x708 v1.2 UPS can be found at:
https://wiki.geekworm.com/X708

  Usage example:

  $ python3 x708asd.py
  $ python3 x708asd.py --debug
  $ python3 x708asd.py --help
"""
import click
import time
import datetime
import logging
from RPi import GPIO

# Constants
DEFAULT_SHUTDOWN_DELAY = 300
DEFAULT_UPS_POWER_LOSS_DETECTION_PIN = 6
DEFAULT_UPS_SHUTDOWN_PIN = 13

# Initialize logger
logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s '
                           '%(levelname)s:%(funcName)s:%(lineno)d '
                           '%(message)s')


def shutdown(pin):
    """Initiate the X708 UPS shutdown.

    Args:
      pin:
        The GPIO pin used to trigger a X708 shutdown.
    """
    GPIO.output(pin, GPIO.HIGH)
    logger.debug('Shutdown pin %s pulled high.', pin)
    time.sleep(3)
    GPIO.output(pin, GPIO.LOW)
    logger.debug('Shutdown pin %s pulled low.', pin)


def power_loss_handler(power_loss_time, timeout, shutdown_pin):
    """A handler for an X708 UPS power loss event.

    In the event a power loss event was detected, the timeout period will
    begin. Once the timeout period is reached, the RaspberryPi will be
    shutdown.

    Args:
      power_loss_time:
        The timestamp in which a power loss event was detected.
      timeout:
        The timeout to take place before initiating a shutdown.
      shutdown_pin:
        The GPIO pin to trigger a shutdown on the X708.
    Returns:
      power_loss_time:
        The updated power_loss_time.
    """
    if power_loss_time is not None:
        current_timeout = (datetime.datetime.now() - power_loss_time).seconds
        if current_timeout > timeout:
            logger.info('Shutdown timer has lapsed, shutting down.')
            shutdown(shutdown_pin)

        # Log in 10 second intervals
        if not current_timeout % 10:
            logger.debug('%s seconds remaining.', timeout - current_timeout)
            time.sleep(1)
    else:
        logger.info('AC power loss detected, starting shutdown timer for %s'
                    ' seconds.', timeout)
        power_loss_time = datetime.datetime.now()
        logger.debug('Shutdown timer started.')
    return power_loss_time

@click.command()
@click.option('-d', '--debug',
              default=False,
              is_flag=True,
              help='Enable debug logging.')
@click.option('-t', '--timeout',
              default=DEFAULT_SHUTDOWN_DELAY,
              show_default=True,
              help='The delay in seconds before a shutdown should be initiated'
                   ' after loosing power.')
@click.option('-p', '--power-pin',
              default=DEFAULT_UPS_POWER_LOSS_DETECTION_PIN,
              show_default=True,
              help='The GPIO pin to detect a power loss event on the X708.')
@click.option('-s', '--shutdown-pin',
              default=DEFAULT_UPS_SHUTDOWN_PIN,
              show_default=True,
              help='The GPIO pin to shutdown the X708.')
def main(debug, timeout, power_pin, shutdown_pin):
    """Initiate monitoring for power loss.

    Args:
      debug:
        A debug command-line flag populated by Click.
      timeout:
        A command-line flag to set the shutdown delay, populated by Click.
      power_pin:
        A command-line flag to set the power loss detection pin, populated by
        Click.
      shutdown_pin:
        A command-line flag to set the pin to initiate a shutdown, populated by
        Click.
    """
    # Set log level
    if debug:
        logger.setLevel(logging.DEBUG)
        logger.debug('Debug logs enabled.')
    else:
        logger.setLevel(logging.INFO)

    # Initialize GPIOs - TODO move this down
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(power_pin, GPIO.IN)
    GPIO.setup(shutdown_pin, GPIO.OUT)

    # Run indefinitely
    logger.info('Begin monitoring for power loss.')
    try:
        logger.debug('Entering infinite loop.')
        power_loss_time = None
        while True:
            # Check if a power loss is actively occuring
            if GPIO.input(power_pin):
                power_loss_time = power_loss_handler(power_loss_time, timeout,
                                                     shutdown_pin)
            elif power_loss_time is not None:
                logger.info('AC power restored, resetting power loss time.')
                power_loss_time = None
    except BaseException:  # pylint: disable=broad-except
        logger.debug('Exiting infinite loop.')
        GPIO.cleanup()
        logger.debug('Cleaned GPIO pin usage.')
    logger.info('Monitoring terminated.')


if __name__ == '__main__':
    main()  # Disable for click. pylint: disable=no-value-for-parameter
