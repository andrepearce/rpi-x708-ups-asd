"""A test script for x708asd.py

This test script is intended to be run using pytest.

  Usage example:

  $ pytest
"""
import time
from unittest.mock import MagicMock, patch, call
from hypothesis import given, strategies as st

MockRPi = MagicMock()
modules = {
    'RPi': MockRPi,
    'RPi.GPIO': MockRPi.GPIO
}
patcher = patch.dict('sys.modules', modules)
patcher.start()

import x708asd  # Imported after patcher. pylint: disable=wrong-import-position


@patch.object(time, 'sleep')
@patch.object(MockRPi.GPIO, 'output')
@given(st.integers())
def test_shutdown(mock_gpio_output, mock_sleep, pin):
    """Test the x708asd shutdown routine.

    Args:
      mock_gpio_output:
        A mock reference for the GPIO output method, populated by patch.
      mock_sleep:
        A mock reference for the timm sleep method, populated by patch.
      pin:
        A pin number to be passed to x708asd.shutdown, populated by hypothesis.
    """
    x708asd.shutdown(pin)

    # Verify sleep calls
    mock_sleep.assert_called_once()
    mock_sleep.reset_mock()

    # Verify GPIO calls
    assert mock_gpio_output.call_count == 2
    calls = [call(13, MockRPi.GPIO.HIGH), call(13, MockRPi.GPIO.LOW)]
    mock_gpio_output.has_calls(calls, any_order=False)
    mock_gpio_output.reset_mock()
